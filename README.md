This repository contains an implementation of the CMake tutorial that can be found at https://cmake.org/cmake-tutorial/

Please read and follow this tutorial and look at the solution when needed.
# Implement step 1 and 2 of the CMake tutorial
## Step 1
The objective is to ask CMake to generate a the file TutorialConfig.h for us in the build directory (`${PROJECT_BINARY_DIR}`) from the file TutorialConfig.h.in we create in the source directory (`${PROJECT_SOURCE_DIR}`).
CMake will replace text in the .h.in with the content of some CMake variables

## Step 2
The objective is to implement our own sqrt routine (which gives a wrong result) in the MathFunctions directory.
The exercice is to tell Cmake to compile this new source as a library and to link it with our executable.
Finally, design an option such that we can choose between the self implemented sqrt and the real sqrt.

# Apply what you have learned on the carrot_juice project and replace the compile.sh script with a proper CMake 
# Implement step 3 and 4 of the CMake tutorial
## Step 3
## Step 4
# Apply what you have learned on the carrot_juice project and add a install target as well as ctest